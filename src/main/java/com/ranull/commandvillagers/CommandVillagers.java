package com.ranull.commandvillagers;

import org.bukkit.plugin.java.JavaPlugin;

import com.ranull.commandvillagers.commands.CommandVillagersCommand;
import com.ranull.commandvillagers.events.Events;

public class CommandVillagers extends JavaPlugin {
	@Override
	public void onEnable() {
		saveDefaultConfig();
		this.getCommand("commandvillagers").setExecutor(new CommandVillagersCommand(this));
		this.getServer().getPluginManager().registerEvents(new Events(this), this);
	}
}