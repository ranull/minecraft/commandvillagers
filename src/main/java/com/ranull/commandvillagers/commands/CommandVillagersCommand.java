package com.ranull.commandvillagers.commands;

import com.ranull.commandvillagers.CommandVillagers;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandVillagersCommand implements CommandExecutor {
	private CommandVillagers plugin;

	public CommandVillagersCommand(CommandVillagers plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		String version = "1.0";
		String author = "Ranull";

		if (args.length < 1) {
			sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GOLD + "CommandVillagers " + ChatColor.GRAY
					+ ChatColor.GRAY + "v" + version);

			sender.sendMessage(ChatColor.GRAY + "/commandvillagers " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
					+ " Plugin info");

			if (sender.hasPermission("commandvillagers.reload")) {
				sender.sendMessage(ChatColor.GRAY + "/commandvillagers reload " + ChatColor.DARK_GRAY + "-"
						+ ChatColor.RESET + " Reload plugin");
			}
			sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);

			return true;
		}

		if (args.length == 1 && args[0].equals("reload")) {
			if (!sender.hasPermission("commandvillagers.reload")) {
				sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "CommandVillagers" + ChatColor.DARK_GRAY
						+ "]" + ChatColor.RESET + " No Permission!");

				return true;
			}

			plugin.reloadConfig();

			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "CommandVillagers" + ChatColor.DARK_GRAY
					+ "]" + ChatColor.RESET + " Reloaded config file!");
		}

		return true;
	}
}