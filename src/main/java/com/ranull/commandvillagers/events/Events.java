package com.ranull.commandvillagers.events;

import com.ranull.commandvillagers.CommandVillagers;
import org.bukkit.Material;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

public class Events implements Listener {
	private CommandVillagers plugin;

	public Events(CommandVillagers plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		if (event.getRightClicked() instanceof Villager) {
			Player player = event.getPlayer();

			if (!player.hasPermission("commandvillagers.use")) {
				return;
			}

			Villager villager = (Villager) event.getRightClicked();

			if (!event.getHand().equals(EquipmentSlot.HAND)) {
				return;
			}

			if (player.getInventory().getItemInMainHand().getType().equals(Material.NAME_TAG)) {
				return;
			}

			if (event.getRightClicked().getCustomName() == null) {
				return;
			}

			String name = villager.getCustomName().toLowerCase().replace(" ", "_");

			event.setCancelled(true);
			// Towny Support
			if (plugin.getConfig().getBoolean("settings.townySupport")) {
				if (event.getRightClicked().getCustomName().toLowerCase().contains("town:")) {
					String town = villager.getCustomName()
							.substring(event.getRightClicked().getCustomName().indexOf(": ") + 2);

					player.performCommand("town spawn " + town);

					return;
				}
			}

			String config = plugin.getConfig().getString("com.rngservers.removeentity.commands." + name);

			if (config == null) {
				return;
			}

			// Run as player
			String run = plugin.getConfig().getString("com.rngservers.removeentity.commands." + name + ".run");
			String command = plugin.getConfig().getString("com.rngservers.removeentity.commands." + name + ".command").replace("$player",
					player.getName());

			if (run.equals("player")) {
				player.performCommand(command);

				return;
			}

			// Run as console
			if (run.equals("console")) {
				ConsoleCommandSender consoleCommandSender = plugin.getServer().getConsoleSender();

				plugin.getServer().dispatchCommand(consoleCommandSender, command);

				return;
			}
		}
	}
}
